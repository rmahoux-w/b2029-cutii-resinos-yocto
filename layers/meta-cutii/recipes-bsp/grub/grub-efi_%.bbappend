do_deploy_prepend_up-board() {
	for file in ${WORKDIR}/grub.cfg*; do
		awk '/^linux/ {$0=$0" efivar_ssdt=goodix"} 1' $file > $file.new
		mv $file.new $file
	done
}
