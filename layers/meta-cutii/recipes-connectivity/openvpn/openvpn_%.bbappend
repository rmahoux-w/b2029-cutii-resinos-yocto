FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI_append = "file://openvpn.service"

SYSTEMD_SERVICE_${PN} = "${@bb.utils.contains("RESIN_CONNECTABLE","1","openvpn-resin.service prepare-openvpn.service","",d)}"

do_install_append() {
    if [ ${RESIN_CONNECTABLE} -eq 1 ]; then
        if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
           install -c -m 0644 ${WORKDIR}/openvpn.service ${D}${systemd_unitdir}/system/openvpn-resin.service
        fi
    fi
}

FILES_${PN} += "${base_libdir}/systemd/system/openvpn.service"
