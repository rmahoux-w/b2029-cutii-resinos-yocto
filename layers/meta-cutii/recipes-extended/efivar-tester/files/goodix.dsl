DefinitionBlock ("goodix.aml", "SSDT", 1, "Goodix", "", 0x00000003)
{
    External (\_SB.PCI0.I2C2, DeviceObj)

    Scope (\_SB.PCI0.I2C2)
    {
        Device (TCS0)
        {
            Name (_ADR, Zero)
            Name (_HID, "GDIX1001")

            Method (_CRS, 0, Serialized)
            {
                Name (RBUF, ResourceTemplate ()
                {
                    I2cSerialBus (0x0014, ControllerInitiated, 0x00061A80,
                                  AddressingMode7Bit, "\\_SB.PCI0.I2C2", 0x00,
                                  ResourceConsumer, , )
                    Interrupt (ResourceConsumer, Edge, ActiveHigh, Exclusive, ,
                    , )
                    {
                        0x00000144,
                    }
                    GpioIo (Exclusive, PullDefault, 0x0000, 0x0000,
                            IoRestrictionNone, "\\_SB.GPO3", 0x00,
                            ResourceConsumer, , )
                    {   // Pin list
                        0x000F
                    }
                    GpioIo (Exclusive, PullDown, 0x0000, 0x0000,
                            IoRestrictionOutputOnly, "\\_SB.GPO3", 0x00,
                            ResourceConsumer, , )
                    {   // Pin list
                        0x0013
                    }
                })
                Return (RBUF)
            }
        }
    }
}
