#!/bin/sh -e

filename=/etc/goodix.aml
name=$(basename ${filename%.*})

EFIVARFS="/sys/firmware/efi/efivars"

[ -d "$EFIVARFS" ] || exit 2

if stat -tf $EFIVARFS | grep -q -v de5e81e4; then
        mount -t efivarfs none $EFIVARFS
fi

# try to pick up an existing GUID
[ -n "$guid" ] || guid=$(find "$EFIVARFS" -name "$name-*" | head -n1 | cut -f2- -d-)

# use a randomly generated GUID
[ -n "$guid" ] || guid="$(cat /proc/sys/kernel/random/uuid)"

# efivarfs expects all of the data in one write
tmp=$(mktemp)
echo -ne "\007\000\000\000" | cat - $filename > $tmp
tmp_md5sum=$(md5sum $tmp | cut -f1 -d' ')
efivar_md5sum=$(md5sum $EFIVARFS/$name-$guid | cut -f1 -d' ')
if [ "$tmp_md5sum" == "$efivar_md5sum" ]; then
	echo "$filename has already been flashed"
	exit
fi
[ -f "$EFIVARFS/$name-$guid" ] && chattr -i $EFIVARFS/$name-$guid
dd if=$tmp of="$EFIVARFS/$name-$guid" bs=$(stat -c %s $tmp)

# reboot required for driver initialization.
reboot
