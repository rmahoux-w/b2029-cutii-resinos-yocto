DESCRIPTION = "Test EFI variables at startup"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "file://efivar-tester.service \
           file://efivar-tester.sh \
           file://goodix.aml"

S = "${WORKDIR}"

inherit systemd

SYSTEMD_SERVICE_${PN} = "${@bb.utils.contains("RESIN_CONNECTABLE","1","efivar-tester.service","",d)}"
SYSTEMD_AUTO_ENABLE = "enable"

do_install () {
   install -d ${D}${sysconfdir} ${D}${systemd_system_unitdir}
   install -m 0644 ${S}/efivar-tester.service ${D}${systemd_system_unitdir}
   install -m 0755 ${S}/efivar-tester.sh ${D}${sysconfdir}
   install -m 0755 ${S}/goodix.aml ${D}${sysconfdir}
}

FILES_${PN} = "${sysconfdir}"
