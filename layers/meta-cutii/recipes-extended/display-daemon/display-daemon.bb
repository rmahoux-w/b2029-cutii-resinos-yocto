FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

DEPENDS = "glib-2.0 glib-2.0-native"

SRC_URI = "file://display-daemon.tar.bz2"

S = "${WORKDIR}/${PN}"

inherit autotools pkgconfig systemd

SYSTEMD_SERVICE_${PN} = "${@bb.utils.contains("RESIN_CONNECTABLE","1","display-daemon.service","",d)}"
SYSTEMD_AUTO_ENABLE = "enable"

FILES_${PN} = "${bindir} \
	${datadir} \
	${sysconfdir}/dbus-1/system.d"
