require resin-custom-config.inc

# resin-image package needs grub-efi deployment
do_configure[depends] += "grub-efi:do_deploy"

IMAGE_INSTALL_append = " i2c-tools \
	pango \
	ttf-liberation-sans-narrow \
	efivar-tester \
	display-daemon \
	fbida"
