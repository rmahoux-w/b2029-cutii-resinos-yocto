require recipes-core/images/resin-image-flasher.bb
require recipes-core/images/resin-image.inc
require resin-custom-config.inc

IMAGE_INSTALL_append = " resin-flasher-config"

RESIN_BOOT_PARTITION_FILES_append_up-board = " \
    grub.cfg_external:/EFI/BOOT/grub.cfg \
    grub.cfg_internal: \
    "
