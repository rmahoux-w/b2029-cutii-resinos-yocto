specific_config() {
	cp ${DEPLOY_DIR_IMAGE}/config.json ${RESIN_BOOT_WORKDIR}/config.json
	echo $(cat ${RESIN_BOOT_WORKDIR}/config.json | jq -S ".date=\"$(date)\"") > ${RESIN_BOOT_WORKDIR}/config.json
}

ROOTFS_POSTPROCESS_COMMAND_append = " specific_config;"
