FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI_append = " \
	file://0001-plymouth-default-theme-is-cutii.patch \
	file://cutii-theme.tar.bz2 \
	file://cutii-linear-theme.tar.bz2 \
	"
do_install_append() {
	# Theme based on progress
	install -d ${D}/${datadir}/plymouth/themes/cutii
	install ${WORKDIR}/cutii/cutii.plymouth ${D}/${datadir}/plymouth/themes/cutii
	install ${WORKDIR}/cutii/*.png ${D}/${datadir}/plymouth/themes/cutii
	# Theme based on time
	install -d ${D}/${datadir}/plymouth/themes/cutii-linear
	install ${WORKDIR}/cutii-linear/cutii-linear* ${D}/${datadir}/plymouth/themes/cutii-linear
	install ${WORKDIR}/cutii-linear/*.png ${D}/${datadir}/plymouth/themes/cutii-linear
	# two-step plugin
	install -d ${D}${nonarch_libdir}/plymouth/
	install ${WORKDIR}/build/src/plugins/splash/two-step/.libs/two-step.so ${D}${nonarch_libdir}/plymouth/
}
