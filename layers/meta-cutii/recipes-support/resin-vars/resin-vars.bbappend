FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI_append = "file://config.json"

inherit deploy

do_deploy() {
    install ${WORKDIR}/config.json ${DEPLOYDIR}
}

addtask deploy before do_package after do_install
