FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# Goodix touchscreen support
SRC_URI += "file://add-goodix-module.cfg"

# realsense driver support
SRC_URI += "file://uvc.cfg \
            file://realsense_hid_linux-yocto_4.9.patch \
            file://realsense_metadata_linux-yocto_4.9.patch \
            file://realsense_powerlinefrequency_control_fix_linux-yocto_4.9.patch \
            file://realsense_camera_formats_linux-yocto_4.9.patch"
