#!/bin/sh

ubuntu_version=$(lsb_release -r | awk '/Release/ {print $2}')
if [ $ubuntu_version != "16.04" ]; then
	echo "Please install Ubuntu 16.04"
fi

package_requirements="gawk wget git-core diffstat unzip texinfo gcc-multilib   \
build-essential chrpath socat cpio python python3 python3-pip python3-pexpect  \
xz-utils debianutils iputils-ping libsdl1.2-dev xterm jq docker.io"
for pkg in $package_requirements; do
	dpkg -s $pkg &> /dev/null
	if [ $? != 0 ]; then
		echo "Please install $pkg"
	fi
done

id -Gn $USER | grep docker &> /dev/null
if [ $? -ne 0 ]; then
	echo "$USER must belongs to docker group."
fi

export TEMPLATECONF=../layers/meta-cutii/conf
resin-yocto-scripts/build/build-device-type-json.sh
if [ $? -ne 0 ]; then
	echo "Board config generation failure."
fi
source layers/poky/oe-init-build-env build
bitbake resin-image-flasher
