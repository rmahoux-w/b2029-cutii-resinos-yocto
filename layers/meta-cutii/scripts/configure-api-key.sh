#!/bin/sh

MOUNT_DIR=$(mktemp -d)

if [ -f resin-io.cfg ]; then
	source ./resin-io.cfg
else
	echo "Please enter APP_ID from https://dashboard.resin.io/apps/APP_ID/devices"
	read APP_ID
	echo "Please enter AUTH_TOKEN/session token from https://dashboard.resin.io/preferences/access-tokens"
	read AUTH_TOKEN
fi

API_KEY=$(curl -H "Authorization: Bearer $AUTH_TOKEN" -X POST https://api.resin.io/application/$APP_ID/generate-api-key | tr -d '"')
echo "API key is $API_KEY"

sudo mount -o loop,offset=4194304,uid=$UID $BUILDDIR/tmp/deploy/images/up-board/resin-image-flasher-up-board.resinos-img $MOUNT_DIR
echo $(cat ${MOUNT_DIR}/config.json | jq -S ".apiKey=\"${API_KEY}\"") > ${MOUNT_DIR}/config.json
sudo umount ${MOUNT_DIR}
